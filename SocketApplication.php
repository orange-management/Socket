<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
namespace Socket;

use Model\CoreSettings;
use phpOMS\Account\AccountManager;
use phpOMS\ApplicationAbstract;
use phpOMS\DataStorage\Cache\Pool as CachePool;
use phpOMS\DataStorage\Database\Pool;
use phpOMS\DataStorage\Session\HttpSession;
use phpOMS\Dispatcher\Dispatcher;
use phpOMS\Event\EventManager;
use phpOMS\Localization\L11nManager;
use phpOMS\Log\FileLogger;
use phpOMS\Module\ModuleManager;
use phpOMS\Router\Router;
use phpOMS\Socket\Client\Client;
use phpOMS\Socket\Server\Server;
use phpOMS\Socket\SocketType;

/**
 * Controller class.
 *
 * @category   Framework
 * @package    Framework
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @license    OMS License 1.0
 * @link       http://orange-management.com
 * @since      1.0.0
 */
class SocketApplication extends ApplicationAbstract
{

    /**
     * Socket type.
     *
     * @var SocketType
     * @since 1.0.0
     */
    private $type;

    /**
     * Constructor.
     *
     * @param array  $config Core config
     * @param string $type   Socket type
     *
     * @throws \Exception
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function __construct(array $config, string $type)
    {
        $this->type = $type;
        $socket     = null;

        if ($type === SocketType::SERVER) {
            $this->dbPool = new Pool();
            $this->dbPool->create('core', $config['db']['core']['masters'][0]);

            // TODO: Create server session manager. Client account has reference to elmeent in here (&$session[$clientID])
            $this->cachePool      = new CachePool($this->dbPool);
            $this->appSettings    = new CoreSettings($this->dbPool->get());
            $this->eventManager   = new EventManager();
            $this->router         = new Router();
            $this->sessionManager = new HttpSession(36000);
            $this->moduleManager  = new ModuleManager($this);
            $this->l11nManager    = new L11nManager();
            $this->accountManager = new AccountManager();
            $this->dispatcher     = new Dispatcher($this);
            $this->logger         = FileLogger::getInstance(ROOT_PATH . '/Logs');

            $modules = $this->moduleManager->getActiveModules();
            $this->moduleManager->initModule($modules);

            $socket = new Server($this);
            $socket->create('127.0.0.1', $config['socket']['master']['port']);
            $socket->setLimit($config['socket']['master']['limit']);
        } elseif ($type === SocketType::CLIENT) {
            $socket = new Client();
            $socket->create('127.0.0.1', $config['socket']['master']['port']);
        } else {
            throw new \InvalidArgumentException('Socket type "' . $type . '" is not supported.');
        }

        $socket->run();
    }
}
